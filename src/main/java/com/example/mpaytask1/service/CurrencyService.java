package com.example.mpaytask1.service;

import com.example.mpaytask1.dto.ValuteDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class CurrencyService {

    public static String TODAY_DATE = LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));

    CacheService cacheService;
    CBARService cbarService;

    public List<ValuteDto> getCurrencyByDate(String date, String currency) {
        if (currency == null || currency.isEmpty()) {
            var data = cacheService.retrieveByDate(date);
            if (data == null) {
                List<ValuteDto> valuteDto = cbarService.retrieveValuteDtoListByDateFromApi(date);
                cacheService.setValuteListByDate(date, valuteDto);
                return valuteDto;
            }
            return data;
        }
        var data = cacheService.retrieveByDateAndCurrency(date, currency);
        if (data == null) {
            ValuteDto valuteDto = cbarService.retrieveValuteDtoByDateAndCurrencyFromApi(date, currency);
            cacheService.setValuteByDateAndCurrency(date, currency, valuteDto);
            return List.of(valuteDto);
        }
        return List.of(data);
    }

    public List<ValuteDto> getTodayCurrency(String currency) {
        return getCurrencyByDate(TODAY_DATE, currency);
    }
}
