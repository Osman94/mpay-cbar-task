package com.example.mpaytask1.service;

import com.example.mpaytask1.dto.ValuteDto;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CacheService {

    private final RedisTemplate<String, ValuteDto> redisTemplate;

    public List<ValuteDto> retrieveByDate(String date) {
        Set<String> keys = redisTemplate.keys(date + ":*");
        if (keys == null || keys.size() < 45) {
            return null;
        }
        return keys.stream()
                .map(key -> redisTemplate.opsForValue().get(key))
                .collect(Collectors.toList());
    }

    public ValuteDto retrieveByDateAndCurrency(String date, String currency) {
        String key = date + ":" + currency;
        return redisTemplate.opsForValue().get(key);
    }

    public void setValuteByDateAndCurrency(String date, String currency, ValuteDto valuteDto) {
        String key = date + ":" + currency;
        redisTemplate.opsForValue().set(key, valuteDto, 30, TimeUnit.DAYS);
    }

    public void setValuteListByDate(String date, List<ValuteDto> valuteDtoList) {
        for (ValuteDto valuteDto : valuteDtoList) {
            String key = date + ":" + valuteDto.getCode(); // Assuming ValuteDto has a getCode() method
            redisTemplate.opsForValue().set(key, valuteDto, 30, TimeUnit.DAYS);
        }
    }

}
