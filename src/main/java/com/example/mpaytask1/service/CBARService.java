package com.example.mpaytask1.service;

import com.example.mpaytask1.dto.CurrencyDto;
import com.example.mpaytask1.dto.ValuteDto;
import com.example.mpaytask1.exception.custom.CurrencyNotFoundException;
import com.example.mpaytask1.exception.custom.DataRetrievalException;
import com.example.mpaytask1.exception.custom.InvalidDateException;
import com.example.mpaytask1.feign.CBARFeignClient;
import com.example.mpaytask1.util.ConverterUtil;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Locale;


@Slf4j
@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class CBARService {

    CBARFeignClient cbarFeignClient;

    public CurrencyDto retrieveCurrencyDtoFromApi(String date) {
        try {
            validateDate(date);
            String xmlContent = cbarFeignClient.getCurrencies(date);
            String jsonFile = ConverterUtil.convertXMLtoJSON(xmlContent);
            return ConverterUtil.convertJSONToDTO(jsonFile, CurrencyDto.class);
        } catch (InvalidDateException e) {
            throw e;
        } catch (Exception e) {
            log.error("Error in retrieving or converting currency data: {}", e.getMessage());
            throw new DataRetrievalException("Failed to retrieve data from API", e);
        }
    }

    public ValuteDto retrieveValuteDtoByDateAndCurrencyFromApi(String date, String currency) {
        var currencyDto = retrieveCurrencyDtoFromApi(date);
        List<ValuteDto> valuteDtoList = ConverterUtil.convertCurrencyDtoToValuteDto(currencyDto);
        return valuteDtoList.stream()
                .filter(valuteDto -> valuteDto.getCode().equals(currency))
                .findFirst()
                .orElseThrow(() -> new CurrencyNotFoundException("Currency not found: " + currency));
    }

    public List<ValuteDto> retrieveValuteDtoListByDateFromApi(String date) {
        var currencyDto = retrieveCurrencyDtoFromApi(date);
        return ConverterUtil.convertCurrencyDtoToValuteDto(currencyDto);
    }

    private void validateDate(String date) {
        try {
            LocalDate inputDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
            LocalDate today = LocalDate.now();

            if (inputDate.isAfter(today)) {
                throw new InvalidDateException("Date is not valid: " + date);
            }
        } catch (DateTimeParseException e) {
            throw new InvalidDateException("Invalid date format: " + date);
        }
    }

}
