package com.example.mpaytask1.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static com.example.mpaytask1.service.CurrencyService.TODAY_DATE;

@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class SchedulerService {
    CBARService cbarService;
    CacheService cacheService;

    public void storeCurrencyForToday() {
        var valuteDto = cbarService.retrieveValuteDtoListByDateFromApi(TODAY_DATE);
        cacheService.setValuteListByDate(TODAY_DATE, valuteDto);
        log.info("Storing today: {} currency by scheduler", TODAY_DATE);
    }
}
