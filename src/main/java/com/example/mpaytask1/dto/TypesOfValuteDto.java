package com.example.mpaytask1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class TypesOfValuteDto /* "Bank metalları", "Xarici valyutalar" */ {

    @JsonProperty("Type")
    private String type;

    @JsonProperty("Valute")
    private List<ValuteDto> valuteDtos;
}
