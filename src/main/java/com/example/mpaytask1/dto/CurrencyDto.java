package com.example.mpaytask1.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class CurrencyDto {

    @JsonProperty("Date")
    private String date;

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Description")
    private String description;

    @JsonProperty("ValType")
    private List<TypesOfValuteDto> typesOfValuteDtos;
}


