package com.example.mpaytask1.contolller;

import com.example.mpaytask1.dto.ValuteDto;
import com.example.mpaytask1.service.CBARService;
import com.example.mpaytask1.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyService currencyService;

    @GetMapping("/today")
    public List<ValuteDto> getTodayCurrency(@RequestParam(required = false) String currency) throws Exception {
        return currencyService.getTodayCurrency(currency);
    }

    @GetMapping("/{date}")
    public List<ValuteDto> getCurrencyByDateAndCode(@PathVariable String date, @RequestParam(required = false) String currency) {
        return currencyService.getCurrencyByDate(date, currency);
    }
}
