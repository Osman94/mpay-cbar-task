package com.example.mpaytask1.scheduler;

import com.example.mpaytask1.service.SchedulerService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CurrencyScheduler {
    SchedulerService schedulerService;

    // Her gun 10.30 da
    @Scheduled(cron = "${application.scheduler.cron.expression}")
    public void storeCurrencyForToday() {
        schedulerService.storeCurrencyForToday();
    }
}
