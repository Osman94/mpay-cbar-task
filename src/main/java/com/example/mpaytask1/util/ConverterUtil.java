package com.example.mpaytask1.util;


import com.example.mpaytask1.dto.CurrencyDto;
import com.example.mpaytask1.dto.TypesOfValuteDto;
import com.example.mpaytask1.dto.ValuteDto;
import com.example.mpaytask1.exception.custom.ConversionException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ConverterUtil {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final XmlMapper xmlMapper = new XmlMapper();

    public static CurrencyDto convertJSONToDTO(String json, Class<CurrencyDto> currencyRatesDtoClass) {
        try {
            log.info("Converting JSON to DTO");
            return objectMapper.readValue(json, currencyRatesDtoClass);
        } catch (Exception e) {
            log.error("Error converting JSON to DTO: {}", e.getMessage());
            throw new ConversionException("Failed to convert JSON to DTO", e);
        }
    }

    public static String convertXMLtoJSON(String xmlContent) {
        try {
            log.info("Converting XML to JSON");
            Object xmlObject = xmlMapper.readValue(xmlContent, Object.class);
            return objectMapper.writeValueAsString(xmlObject);
        } catch (Exception e) {
            log.error("Error converting XML to JSON: {}", e.getMessage());
            throw new ConversionException("Failed to convert XML to JSON", e);
        }
    }

    public static List<ValuteDto> convertCurrencyDtoToValuteDto(CurrencyDto currencyDto) {
        List<ValuteDto> valuteDtos = new ArrayList<>();
        for (TypesOfValuteDto typesOfValuteDto : currencyDto.getTypesOfValuteDtos()) {
            valuteDtos.addAll(typesOfValuteDto.getValuteDtos());
        }
        return valuteDtos;
    }

}
