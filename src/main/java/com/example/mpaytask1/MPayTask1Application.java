package com.example.mpaytask1;

import com.example.mpaytask1.dto.ValuteDto;
import com.example.mpaytask1.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;

@SpringBootApplication
@EnableFeignClients
@EnableScheduling
@Slf4j
public class MPayTask1Application implements CommandLineRunner {

    private final CacheService cacheService;

    public MPayTask1Application(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    public static void main(String[] args) {
        SpringApplication.run(MPayTask1Application.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        log.info("Running MPayTask1Application");
        //  var valuteDtoList = cacheService.retrieveByDate("26.06.2024");
        // var valuteDto = cacheService.retrieveByDateAndCurrency("26.06.2024", "EUR");
        //System.out.println(valuteDto);
    }
}
