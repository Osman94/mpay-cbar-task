package com.example.mpaytask1.exception;

import com.example.mpaytask1.exception.custom.ConversionException;
import com.example.mpaytask1.exception.custom.DataRetrievalException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception exception) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(exception.getMessage());
    }

    @ExceptionHandler(ConversionException.class)
    public ResponseEntity<Object> handleConversionException(ConversionException exception) {
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body("Error converting currency data: " + exception.getMessage());
    }

    @ExceptionHandler(DataRetrievalException.class)
    public ResponseEntity<Object> handleDataRetrievalException(DataRetrievalException exception) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body("Error retrieving currency data: " + exception.getMessage());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<?> handleIllegalArgumentException(IllegalArgumentException e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
}