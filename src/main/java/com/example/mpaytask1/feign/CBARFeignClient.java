package com.example.mpaytask1.feign;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cbarClient", url = "https://www.cbar.az")
public interface CBARFeignClient {

    @GetMapping("/currencies/{date}.xml")
    String getCurrencies(@PathVariable("date") String date);
}
